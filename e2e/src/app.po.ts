import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('npm ');
  }

  getParagraphText() {
    return element(by.deepCss('app-root ion-content')).getText();
  }
}

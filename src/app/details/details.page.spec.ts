import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from "@angular/core";
import {
  async,
  ComponentFixture,
  TestBed,
  inject,
} from "@angular/core/testing";
import { HttpClientModule } from "@angular/common/http";
import { DetailsPage } from "./details.page";
import { NewsService } from "../news.service";
import { By } from '@angular/platform-browser';

describe("DetailsPage", () => {
  let component: DetailsPage;
  let fixture: ComponentFixture<DetailsPage>;
  let debugEl: DebugElement;
  let htmlEl: HTMLElement;  

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientModule],
    }).compileComponents();
  }));

  beforeEach(inject([NewsService], (newsService) => {
    const article: any = {
      author: "Test Author",
      publishedAt: "2020-04-14T06:46:39Z",
      urlToImage: "Test.jpg",
      title: "Test Title",
      content: "Test content",
    };
    newsService.currentNews = article;
    fixture = TestBed.createComponent(DetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  afterEach(() => {
    component = null;
    fixture.destroy();
    debugEl = null;
    htmlEl = null;
  });

  it("should create details page", () => {
    expect(component).toBeTruthy();
  });

  it('should contain title on header', () => {
    debugEl = fixture.debugElement.query(By.css('ion-title'));
    htmlEl = debugEl.nativeElement;
    expect(htmlEl.textContent).toContain('Test');
  });

  it('should contain image source', () => {
    debugEl = fixture.debugElement.query(By.css('ion-img'));
    htmlEl = debugEl.nativeElement;
    expect(debugEl.properties.src).toEqual('Test.jpg');
  });

  it('should have author', () => {
    debugEl = fixture.debugElement.query(By.css('ion-title'));
    htmlEl = debugEl.nativeElement;
    expect(htmlEl.textContent).toContain('Test Author');
  });

  it('should have publishedAt on header', () => {
    debugEl = fixture.debugElement.query(By.css('ion-title'));
    htmlEl = debugEl.nativeElement;
    expect(htmlEl.textContent).toContain('2020-04-14T06:46:39Z');
  });

  it('should have publishedAt on header', () => {
    debugEl = fixture.debugElement.query(By.css('ion-card-content'));
    htmlEl = debugEl.nativeElement;
    expect(htmlEl.textContent).toContain('Test content');
  });
  
});

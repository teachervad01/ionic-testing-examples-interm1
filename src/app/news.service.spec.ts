import { TestBed, async } from "@angular/core/testing";
import { HttpClientModule } from "@angular/common/http";
import { NewsService } from "./news.service";

describe("NewsService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    })
  );

  it("should be created", () => {
    const service: NewsService = TestBed.get(NewsService);
    expect(service).toBeTruthy();
  });

  it("should get http data", async(() => {
    const service: NewsService = TestBed.get(NewsService);
    service
      .getData("top-headlines?country=us&category=business")
      .subscribe((data) => {
        const result = data.status;
        const items: any = data.articles;
        console.log(items);
        expect(result).toEqual("ok");
        expect(Array.isArray(items)).toBeTruthy();
        expect(items.length).toBeGreaterThan(0);
      });
  }));

  it('should contain author, publishedAt, urlToImage', async(() => {
    const service: NewsService = TestBed.get(NewsService);
    service
      .getData("top-headlines?country=us&category=business")
      .subscribe((data) => {
        const items = data.articles;
        const firstArticle = items[0];
        expect(typeof firstArticle).toEqual('object');
        expect(Object.keys(firstArticle)).toContain('author');
        expect(Object.keys(firstArticle)).toContain('publishedAt');
        expect(Object.keys(firstArticle)).toContain('urlToImage');
      });
  }));

  it('should contain the current news', () => {
    const service: NewsService = TestBed.get(NewsService);
    const article: any = {
      author: "Test Author",
      publishedAt: "2020-04-14T06:46:39Z",
      urlToImage: "Test.jpg",
      title: "Test Title",
      content: "Test content",
    }
    service.currentNews = article;
    expect(typeof (service.currentNews)).toEqual('object');
    expect(Object.keys(service.currentNews)).toContain('author');
    expect(Object.keys(service.currentNews)).toContain('publishedAt');
    expect(Object.keys(service.currentNews)).toContain('urlToImage');
    expect(service.currentNews.author).toContain('Test Author');
    expect(service.currentNews.publishedAt).toContain('2020-04-14T06:46:39Z');
    expect(service.currentNews.urlToImage).toContain('Test.jpg');
  });
});